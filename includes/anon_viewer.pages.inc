<?php

/**
 * @file
 * Page callbacks for anon viewer.
 */

/**
 * Callback for viewing the anonymous user. Finally!
 */
function anon_viewer_view_anon() {
  $output = '<p>' . t('We are Anonymous. We are Legion. We do not forgive. We do not forget. Expect us.') . '</p>';

  if (variable_get('anon_viewer_mask')) {
    $image_url = url(drupal_get_path('module', 'anon_viewer') . '/images/mask.jpg', array('absolute' => TRUE));
    $output .= '<img src="' . $image_url  .'" alt="' . t('Guy Fawkes Mask') . '" />';
  }

  return $output;
}
