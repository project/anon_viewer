<?php

/**
 * @file
 * Admin callbacks for anon viewer.
 */

/**
 * Callback for settings form.
 */
function anon_viewer_settings($form, &$form_state) {
  $form['anon_viewer_mask'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show mask'),
    '#default_value' => variable_get('anon_viewer_mask', FALSE),
  );

  return system_settings_form($form);
}
